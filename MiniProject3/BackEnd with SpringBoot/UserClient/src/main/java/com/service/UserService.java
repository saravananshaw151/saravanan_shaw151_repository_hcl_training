package com.service;

import java.util.List;
import java.util.Objects;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.bean.UserEntity;
import com.dao.UserDao;

@Service
public class UserService {

	@Autowired
	UserDao userDao;
	
	

	public String signUpUser(UserEntity user) {
		UserEntity ue = userDao.findByEmailAndPassword(user.getUseremail(), user.getPassword());
		UserEntity ur = userDao.findByEmailOrPassword(user.getUseremail(), user.getPassword());
		if (Objects.nonNull(ue)) {
			return "Username Already Taken, Try with different one and should be unique";

		} else if (Objects.nonNull(ur)) {
			return "Username Already Taken, Try with different one and should be unique";
		} else {
			userDao.save(user);
			return "User Registered Successfully";
		}
	}

	public String signInUser(UserEntity user) {
		UserEntity userent = userDao.findByEmailAndPassword(user.getUseremail(), user.getPassword());
		if (Objects.nonNull(userent)) {
			int len = user.getUseremail().length() - 10;
			return "Welcome user " + user.getUseremail().substring(0, len) + " You are Logged IN Successfully ";
		} else {
			return "Incorrect Password or Username";
		}
	}

	public String createUser(UserEntity user) {

		if (userDao.existsById(user.getId())) {
			return "This is ID already Exists, Please Give different ID";
		} else {
			userDao.save(user);
			return "User Created successfully";
		}
	}

	public List<UserEntity> retrieveUser() {
		return userDao.findAll();
	}

	public String updateUser(UserEntity user) {

		if (!userDao.existsById(user.getId())) {
			return "This ID not Exist, Please give correct ID";
		} else {
			UserEntity us = userDao.getById(user.getId());
			us.setUserfullname(user.getUserfullname());
			us.setContact(user.getContact());
			us.setGender(user.getGender());
			us.setAddress(user.getAddress());
			us.setCity(user.getCity());
			us.setState(user.getState());
			userDao.save(us);
			return "User Details updated Successfully";
		}
	}

	public String deleteUser(int id) {
		if (!userDao.existsById(id)) {
			return "This ID not exists, Please give Valid ID";
		} else {
			userDao.deleteById(id);
			return "User in the Id number " + id + " has Deleted successfully";
		}
	}

}
