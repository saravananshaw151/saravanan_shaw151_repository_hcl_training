package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.bean.UserEntity;
import com.service.UserService;

@RestController
@RequestMapping("/UserActivity")
@CrossOrigin
public class UserController {
	
	@Autowired
	UserService userService;

	@PostMapping(value="signUp",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String signUp(@RequestBody UserEntity user) {
		return userService.signUpUser(user);
	}
	
	@PostMapping(value="signIn",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String signIn(@RequestBody UserEntity user) {
		return userService.signInUser(user);
	}
	
	@GetMapping(value="logout")
	public String logout() {
		return "Logged Out successfully"; 
	}
	
	@PostMapping(value = "createUser",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String CreateUserInfo(@RequestBody UserEntity user) {
		return userService.createUser(user);
	}
	
	@GetMapping(value="retrieveUsers",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<UserEntity> retrieveUserDetails(){
		return userService.retrieveUser();
	}
	
	@PutMapping(value="updateUser")
	public String updateUserdetails(@RequestBody UserEntity user) {
		return userService.updateUser(user);
		
	}
	
	@DeleteMapping(value="deleteUser/{id}")
	public String deleteUserDetails(@PathVariable("id") int id) {
		return userService.deleteUser(id);
	}
}
