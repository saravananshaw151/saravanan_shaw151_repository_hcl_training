package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.UserInformation;
import com.service.UserService;



@RestController
@RequestMapping("/UserCRUD")
public class UserCrudOperations {

	@Autowired
	UserService userService;
	
	
	@PostMapping(value = "createUser",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String CreateUserInfo(@RequestBody UserInformation user) {
		return userService.creatingUser(user);
	}
	
	@GetMapping(value="retrieveUsers",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<UserInformation> retrieveUserDetails(){
		return userService.retreiveUserDetails();
	}
	
	@PatchMapping(value="updateUser")
	public String updateUserdetails(@RequestBody UserInformation user) {
		return userService.updateUserDetails(user);
		
	}
	
	@DeleteMapping(value="deleteUser/{id}")
	public String deleteUserDetails(@PathVariable("id") int id) {
		return userService.deleteUserDetails(id);
	}
	
	
	
	
}
