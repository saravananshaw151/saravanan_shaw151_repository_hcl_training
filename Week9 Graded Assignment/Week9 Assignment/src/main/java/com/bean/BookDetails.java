package com.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bookdetails")
public class BookDetails {

	@Id
	private int  id;
	@Column(name="bookname")
	private String bookName;
	private float bookprice;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public float getBookprice() {
		return bookprice;
	}
	public void setBookprice(float bookprice) {
		this.bookprice = bookprice;
	}
	@Override
	public String toString() {
		return "BookDetails [id=" + id + ", bookName=" + bookName + ", bookprice=" + bookprice + "]";
	}
	
	
	
}
