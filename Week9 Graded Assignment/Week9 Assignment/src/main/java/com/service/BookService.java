package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.BookDetails;
import com.dao.BookDao;

@Service
public class BookService {

	@Autowired
	BookDao bookDao;
	
	public String createNewBook(BookDetails book) {
		if(bookDao.existsById(book.getId())) {
		 return "Book Id should be unique,try with different ID";	
		}
		else {
			bookDao.save(book);
		  return "Book Created Successfully";	
		}
	}
	
	public List<BookDetails> retreiveBooks(){
		return bookDao.findAll();
	}
	
	public String updateBook(BookDetails book) {
		if(!bookDao.existsById(book.getId())) {
			return "Book not available in this ID";
		}
		else {
			BookDetails bk = bookDao.getById(book.getId());
			bk.setBookName(book.getBookName());
			bk.setBookprice(book.getBookprice());
			bookDao.saveAndFlush(bk);
			return "Book Updated Successfully";
		}
	}
	
	public String deleteBook(int id) {
		if(!bookDao.existsById(id)) {
			return "Book not exists in the ID";
		}
		else {
			bookDao.deleteById(id);
			return "Book Deleted Successfully";
		}
	}
    	
}
