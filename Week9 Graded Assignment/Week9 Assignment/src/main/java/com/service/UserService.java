package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.UserInformation;
import com.dao.UserDao;

@Service
public class UserService {

	@Autowired
	UserDao userDao;
	
	public String creatingUser(UserInformation user) {
		if(userDao.existsById(user.getId())) {
			return "User Id must be Unique, Check once again";
		}
		else {
			userDao.save(user);
			return "User Details stored successfully";
		}
	}
	
	public List<UserInformation> retreiveUserDetails(){
		return userDao.findAll();
	}
	
	public String updateUserDetails(UserInformation user) {
		if(!userDao.existsById(user.getId())) {
			return "User not available in this ID";
		}
		else {
			UserInformation userinfo = userDao.getById(user.getId());
			userinfo.setUsername(user.getUsername());
			userinfo.setContact(user.getContact());
			userinfo.setCity(user.getCity());
			userDao.save(userinfo);
			return "User details updated successfully";
		}
	}
	public String deleteUserDetails(int id) {
		if(!userDao.existsById(id)) {
			return "User Id not available";
		}
		else {
			userDao.deleteById(id);
			return "User detail deleted successfully";
		}
	}
	
}
