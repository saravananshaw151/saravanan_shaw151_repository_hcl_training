package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bean.UserInformation;
@Repository
public interface UserDao extends JpaRepository<UserInformation, Integer>{

		
}
