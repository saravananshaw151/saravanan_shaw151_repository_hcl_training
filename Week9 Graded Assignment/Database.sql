create database SpringbootwithRestApi;
use Springbootwithrestapi;

create table admininformation(username varchar(50) not null primary key, password varchar(50) not null);

insert into admininformation values("ashwinkumar123@gmail.com","ashwink927");
insert into admininformation values("karthick7289@gmail.com","Karthick272");
insert into admininformation values("jerwinkumar739@gmail.com","jerwin2782");
insert into admininformation values("prakash272@gmail.com","prakash876");

create table userinformation(ID int not null primary key,username varchar(50),password varchar(50));

insert into userinformation values(1,"jaiganesh827@gmail.com","jaiganesh2687");
insert into userinformation values(2,"bhargavraju123@gmail.com","bhargavraju2782");
insert into userinformation values(3,"vinay298@gmail.com","vinay628");
insert into userinformation values(4,"ganesh891@gmail.com","ganesh289");
insert into userinformation values(5,"nagarajan1667@gmail.com","nagarajan879");

create table bookdetails(ID int not null primary key,bookname varchar(50),bookprice float);

insert into bookdetails values(100,"JungleBook",760);
insert into bookdetails values(101,"Interstellor",863);
insert into bookdetails values(102,"Vision is my mission",450);
insert into bookdetails values(103,"Vinatge comics",390);
insert into bookdetails values(104,"James Bond",780);
insert into bookdetails values(105,"Mercury",1900);
insert into bookdetails values(106,"Ice Age",650);
insert into bookdetails values(107,"Harry Potter",2500);

select *from admininformation;
select *from userinformation;
select *from bookdetails;

