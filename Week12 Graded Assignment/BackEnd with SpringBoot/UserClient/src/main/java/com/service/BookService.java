package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.BookEntity;
import com.dao.BookDao;

@Service
public class BookService {

	@Autowired
	BookDao bookDao;
	
	public String storeBook(BookEntity book) {

		if (bookDao.existsById(book.getBid())) {
			return "This ID already Exists, Please Give different ID";
		} else {
			bookDao.save(book);
			return "Book Stored successfully";
		}
	}

	public List<BookEntity> retrieveBooks() {
		return bookDao.findAll();
	}

	public String updateBook(BookEntity book) {

		if (!bookDao.existsById(book.getBid())) {
			return "This ID not Exist, Please give correct ID";
		} else {
			BookEntity bk = bookDao.getById(book.getBid());
			bk.setBookname(book.getBookname());
			bk.setAuthor(book.getAuthor());
			bk.setBookgenre(book.getBookgenre());
			bk.setBookimage(book.getBookimage());
			bk.setPrice(book.getPrice());
			bookDao.save(bk);
			return "Book Details updated Successfully";
		}
	}

	public String deleteBook(int id) {
		if (!bookDao.existsById(id)) {
			return "This ID not exists, Please give Valid ID";
		} else {
			bookDao.deleteById(id);
			return "Book in the Id number " + id + " has Deleted successfully";
		}
	}
	
}
