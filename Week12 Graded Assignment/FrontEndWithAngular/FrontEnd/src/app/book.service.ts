import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BookEntity } from './book-entity';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(public http:HttpClient) { }

  getAllBooks():Observable<BookEntity[]>{
    return this.http.get<BookEntity[]>("http://localhost:8181/BookCRUD/getallbooks");
  }
  storeBook(book:BookEntity):Observable<string>{
      return this.http.post("http://localhost:8181/BookCRUD/storeBook",book,{responseType:'text'});
  }

  deleteBookInfo(uid:number):Observable<string>{
    return this.http.delete("http://localhost:8181/BookCRUD/deleteBookInfo/"+uid,{responseType:'text'});
  }

  updateBookInfo(book:any):Observable<string>{
    return this.http.put("http://localhost:8181/BookCRUD/updateBookInfo",book,{responseType:'text'});
  }


}
