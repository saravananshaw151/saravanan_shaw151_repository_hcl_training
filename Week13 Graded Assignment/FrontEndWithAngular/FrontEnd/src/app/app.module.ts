import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminControlComponent } from './admin-control/admin-control.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsercontrolComponent } from './usercontrol/usercontrol.component';
import { AdminhomepageComponent } from './adminhomepage/adminhomepage.component';
import { UserhomepageComponent } from './userhomepage/userhomepage.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminControlComponent,
    UsercontrolComponent,
    AdminhomepageComponent,
    UserhomepageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,HttpClientModule,ReactiveFormsModule,FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
