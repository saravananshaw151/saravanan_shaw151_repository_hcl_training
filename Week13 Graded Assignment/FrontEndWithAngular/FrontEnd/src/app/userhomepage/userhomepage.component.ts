import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userhomepage',
  templateUrl: './userhomepage.component.html',
  styleUrls: ['./userhomepage.component.css']
})
export class UserhomepageComponent implements OnInit {
  
  userlogin:string="";

  constructor(public route:Router) { }

  ngOnInit(): void {
    let obj = sessionStorage.getItem("usname");
    if(obj!=null){
      this.userlogin=obj;
    }
  }
  logout() {
    sessionStorage.removeItem("usname");
    this.route.navigate(["userlogin"]);
}


}
