package com.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.bean.BookDetails;



@RestController
@RequestMapping("/BookCRUD")
@CrossOrigin
public class BookCrudController {

	@Autowired
	RestTemplate restTemplate;

	
	@GetMapping(value="getallbooks")
	public List<Object> getAllBooks(){
		String Url="http://user-service:8282/BookCRUD/getBooks";
		Object[] res = restTemplate.getForObject(Url, Object[].class);
		return Arrays.asList(res);
	}
	
	@PostMapping(value = "storeBook",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBook(@RequestBody Object obj) {
		String Url="http://user-service:8282/BookCRUD/storeBook/";
		ResponseEntity<String> user= restTemplate.postForEntity(Url, obj, String.class);
		return user.getBody();
	}

  

	@PutMapping(value = "updateBookInfo", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String updateBookDetails(@RequestBody BookDetails book ) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<BookDetails> entity = new HttpEntity<>(book,headers);
		String url = "http://user-service:8282/BookCRUD/updateBook";
		return restTemplate.exchange(url, HttpMethod.PUT, entity, String.class).getBody();
		
	}
	
	
	
	@DeleteMapping(value = "deleteBookInfo/{id}")
	public String deleteBookInfo(@PathVariable("id") int id)
	{
		  HttpHeaders headers = new HttpHeaders();
	      headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	      HttpEntity<BookDetails> entity = new HttpEntity<>(headers);
		String url="http://user-service:8282/BookCRUD/deleteBook/"+id;
		return restTemplate.exchange(url , HttpMethod.DELETE, entity, String.class).getBody();
		
	}
	
}
