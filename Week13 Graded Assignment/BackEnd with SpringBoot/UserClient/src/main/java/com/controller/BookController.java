package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.BookEntity;
import com.service.BookService;

@RestController
@RequestMapping("/BookCRUD")
@CrossOrigin
public class BookController {

	@Autowired
	BookService bookSer;
	
	@PostMapping(value="storeBook",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBook(@RequestBody BookEntity book) {
		return bookSer.storeBook(book);
	}
	
	@GetMapping(value="getBooks",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<BookEntity> getAllBooks(){
		return bookSer.retrieveBooks();
	}
	
	@PutMapping(value="updateBook")
	public String updateBookDetails(@RequestBody BookEntity book) {
		return bookSer.updateBook(book);
	}
	
	@DeleteMapping(value="deleteBook/{bid}")
	public String deleteBook(@PathVariable("bid") int id) {
		return bookSer.deleteBook(id);
	}
	
}
