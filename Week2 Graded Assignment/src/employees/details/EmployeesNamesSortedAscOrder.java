package employees.details;

import java.util.Comparator;

public class EmployeesNamesSortedAscOrder implements Comparator<EmployeeRecords> {

	@Override
	public int compare(EmployeeRecords o1, EmployeeRecords o2) {
		
		return o1.getEmpname().compareTo(o2.getEmpname());
	}
    
	
}
