package employees.details;
import java.util.ArrayList;
import java.util.Collections;
public class MainClass {

	public static void main(String[] args) {
		
		
		ArrayList<EmployeeRecords> emobj = new ArrayList<EmployeeRecords>();
		
	    try {
		
		emobj.add(new EmployeeRecords(3, "Harish", 23, 45000, "It department", "Chennai") );
		emobj.add(new EmployeeRecords(8, "Murugan", 28, 65000, "Manufacturing dept", "Kanchipuram"));
		emobj.add(new EmployeeRecords(6, "Prakash", 25, 74000, "HR dept", "Nagpur"));
		emobj.add(new EmployeeRecords(9, "AKash", 32, 50000, "Sales dept", "Vandalur"));
		emobj.add(new EmployeeRecords(2, "venky", 30, 30000, "Production", "Kanchipuram"));
	    emobj.add(new EmployeeRecords(5, "Praveen", 45, 78000, "Admin dept", "Mexico"));
		int flag=0;
		for(EmployeeRecords m:emobj) {
			
		
		     if(m.getEmpid()<0 ||m.getEmpname()==null||m.getAge()<0 || m.getSalary()<0||m.getDepartment()==null||m.getCity()==null) {
	    
	    	 flag=1;
	           	
		     }
		
		  }
		if(flag==1) {
			throw new ExceptionThrowingClass("illegal argument Exception- enter details in proper manner, values should not less than 0 and not to be null");
		}
		else {
			System.out.println("Entered details are eligible to add to the List");
		}
		System.out.println();
	    
		
		System.out.println("All Employees Details ");
		for(EmployeeRecords e:emobj) {
			System.out.println(e);
		}
		
		System.out.println();
		System.out.println();
		Collections.sort(emobj,new EmployeesNamesSortedAscOrder());
		System.out.println("Names of all the employees in sorted alphabetical order");
		
		for(EmployeeRecords e:emobj) {
			System.out.println(e);
		}
		
		
		CityCountOfEmployees ct = new CityCountOfEmployees();
		ct.citynamecount(emobj);
		
		GetsalaryOfEmployees gt = new GetsalaryOfEmployees();
		gt.Getsalarydetails(emobj);
		
	    }
	    catch(Exception e) {
	    	System.out.println(e);
	    }
		
	}

	
}
