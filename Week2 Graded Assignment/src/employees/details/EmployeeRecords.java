package employees.details;


public class EmployeeRecords {

	
	private int empid;
	private String empname;
	private int age;
	private int salary;
	private String department;
	private String city;
	
	
	
	
	public EmployeeRecords() {
		super();
		// TODO Auto-generated constructor stub
	}
	public EmployeeRecords(int empid, String empname, int age, int salary, String department, String city) {
		super();
		this.empid = empid;
		this.empname = empname;
		this.age = age;
		this.salary = salary;
		this.department = department;
		this.city = city;
	}
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	public String getEmpname() {
		return empname;
	}
	public void setEmpname(String empname) {
		this.empname = empname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Override
	public String toString() {
		return "EmployeeRecords [empid=" + empid + ", empname=" + empname + ", age=" + age + ", salary=" + salary
				+ ", department=" + department + ", city=" + city + "]";
	}
	
	
	
	
	
	
	
	
}
