create database Travel_saravanan_shaw;
use Travel_saravanan_shaw;


create table PASSENGER
 (Passenger_id int primary key,Passenger_name varchar(20),Category enum('AC','NON-AC'),Gender enum('Male','Female'),Boarding_City varchar(30),Destination_City varchar(30),Distance int,Bus_Type enum('Sleeper','Sitting'));


create table PRICE(Bus_Type enum('Sleeper','Sitting'),Distance int, Price int);

insert into passenger values(1,'Sejal','AC','Female','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values(2,'Anmol','NON-AC','Male','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values(3,'Pallavi','AC','Female','panaji','Bengaluru',600,'Sleeper');
insert into passenger values(4,'Khusboo','AC','Female','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values(5,'Udit','NON-AC','Male','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values(6,'Ankur','AC','Male','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values(7,'Hemant','NON-AC','Male','panaji','Mumbai',700,'Sleeper');
insert into passenger values(8,'Manish','NON-AC','Male','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values(9,'Piyush','AC','Male','Pune','Nagpur',700,'Sitting');
insert into passenger values(10,'Kavi','NON-AC','Female','Bhutan','Vijayawada',1200,'Sitting');
insert into passenger values(11,'Nikhil','AC','Male','taiwan','Srinagar',1500,'Sitting');


select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',1500,2890);
insert into price values('Sleeper',380,934);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;

select count(case when gender='female' then 1 end) as Female_count_for_travelled_600kms, count(case when gender='male' then 1 end) as Male_count_for_travelled_600kms from passenger where distance>=600;

select min(price) as Minimum_ticket_price_for_sleeper from price where bus_type = 'sleeper';

select *from passenger where passenger_name like 's%';

select pa.passenger_name,pa.boarding_city,pa.destination_city,pa.Bus_type,pr.price from passenger pa,price pr where pa.distance=pr.distance and pa.bus_type=pr.bus_type;

select pa.passenger_name,pr.price as Price_for_travelled_1000kms_and_sitting_category from passenger pa,price pr where pa.distance=pr.distance and pa.bus_type=pr.bus_type and pa.distance>=1000 and pa.bus_type='sitting';

select pr.bus_type as Bus_type_charges_for_pallavi ,pr.price from price pr,passenger pa where pa.passenger_name='pallavi' and pa.distance=pr.distance ;

select distance as Unique_distances_in_passenger_table from passenger group by distance order by distance desc;

select passenger_name,round(distance*100/(select sum(distance) from passenger)) as percentage_of_distance_travelled from passenger ;

create view pass_in_AC_bus as select passenger_name,category from passenger where category='AC';
select *from pass_in_ac_bus;


delimiter !
create procedure Total_in_sleeper()
begin
select count(bus_type) as Total__passenger_in_SleeperType from passenger where bus_type='sleeper';
end !
call Total_in_sleeper() !
delimiter ;


select *from passenger limit 5,5;









