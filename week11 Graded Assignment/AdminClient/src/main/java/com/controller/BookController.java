package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.BookEntity;
import com.service.BookService;

@RestController
@RequestMapping("/BookCRUD")
public class BookController {

	@Autowired
	BookService bookService;
	
	@PostMapping(value = "createBook",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String CreateUserInfo(@RequestBody BookEntity book) {
		return bookService.createBook(book);
	}
	
	@GetMapping(value="retrieveBooks",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<BookEntity> retrieveUserDetails(){
		return bookService.retrieveBooks();
	}
	
	@PatchMapping(value="updateBook")
	public String updateUserdetails(@RequestBody BookEntity book) {
		return bookService.updateBook(book);
	}
	
	@DeleteMapping(value="deleteBook/{id}")
	public String deleteUserDetails(@PathVariable("id") int id) {
		return bookService.deleteBook(id);
	}
}
