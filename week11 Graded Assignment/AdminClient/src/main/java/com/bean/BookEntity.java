package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bookentity")
public class BookEntity {

 @Id	
 private int id;
 private String bookname;
 private String author;
 private float price;
 
 
public BookEntity() {
	super();
	// TODO Auto-generated constructor stub
}
public BookEntity(int id, String bookname, String author, float price) {
	super();
	this.id = id;
	this.bookname = bookname;
	this.author = author;
	this.price = price;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getBookname() {
	return bookname;
}
public void setBookname(String bookname) {
	this.bookname = bookname;
}
public String getAuthor() {
	return author;
}
public void setAuthor(String author) {
	this.author = author;
}
public float getPrice() {
	return price;
}
public void setPrice(float price) {
	this.price = price;
}
@Override
public String toString() {
	return "BookEntity [id=" + id + ", bookname=" + bookname + ", author=" + author + ", price=" + price + "]";
}
	
}
