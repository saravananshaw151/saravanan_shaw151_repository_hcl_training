package com.bookservice.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bean.BookEntity;
import com.dao.BookDao;
import com.service.BookService;

@ExtendWith(MockitoExtension.class)
class BookServiceTest {

	@Mock
	BookDao bookDao;
	
	@InjectMocks
	BookService bookService;
	
	
	@Test
	void testCreateBook() {
	//	fail("Not yet implemented");
		Mockito.when(bookDao.existsById(0)).thenReturn(false);
		
		String result=bookService.createBook(new BookEntity());
		Assertions.assertEquals("Book Created successfully",result);

	}
	@Test
	void testRetrieveBooks() {
		//fail("Not yet implemented");
		List<BookEntity> listOfBooks = new ArrayList<>();
		
		listOfBooks.add(new BookEntity(103,"Lord Of The Rings","Kelvin Jhonse",1600));
		listOfBooks.add(new BookEntity(104,"Hitler Story","Greigur wince",1280));
		Mockito.when(bookDao.findAll()).thenReturn(listOfBooks);
		Assertions.assertTrue(bookService.retrieveBooks().get(0).getId()==103);
		Assertions.assertTrue(bookService.retrieveBooks().get(0).getBookname().equals("Lord Of The Rings"));
		Assertions.assertTrue(bookService.retrieveBooks().get(0).getAuthor().equals("Kelvin Jhonse"));
		Assertions.assertTrue(bookService.retrieveBooks().get(0).getPrice()==1600);
	}
	@Test
	void testUpdateBook() {
		//fail("Not yet implemented");
        Mockito.when(bookDao.existsById(0)).thenReturn(false);
		
		String result=bookService.updateBook(new BookEntity());
		Assertions.assertEquals("This ID not Exist, Please give correct ID",result);

	}

	@Test
	void testDeleteBook() {
		//fail("Not yet implemented");
        Mockito.when(bookDao.existsById(0)).thenReturn(true);
		
		String result=bookService.deleteBook(0);
		Assertions.assertEquals("Book in the Id number 0 has Deleted successfully",result);		
	}

}
