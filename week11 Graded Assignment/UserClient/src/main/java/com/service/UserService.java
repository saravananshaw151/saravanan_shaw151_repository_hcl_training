package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.bean.UserEntity;
import com.dao.UserDao;

@Service
public class UserService {


	@Autowired
	UserDao userDao;
    
	public String signUpUser(UserEntity user) {
		if(userDao.existsById(user.getId())) {
			return "User already exists, Try different Id";
		}
		else {
			userDao.save(user);
			return "User Registered Successfully";
		}
	}
	
	public String signInUser(UserEntity user) {
		if(userDao.existsById(user.getId())) {
			return "Welcome User, "+user.getUsername()+" you have logged In successfully";
		}
		else {
			return "Incorrect credentials";
		}
	}
	
}
