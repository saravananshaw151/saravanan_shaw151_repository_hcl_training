package restaurant.app;


import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class BillDatabase {
	// bill databasee contains each bill with timestamp
		private static List<Bill> billDb= new LinkedList<>();

		//adding bill in bill database
		public static synchronized void addBill(Bill bill) {
			billDb.add(bill);
		}
		// returns today bills
		public synchronized List<Bill> getTodayBill() throws UserException{
			if(!billDb.isEmpty()) {
				List<Bill> todayBillList= new LinkedList<>();
				LocalDate todayDate =LocalDate.now();
				for(Bill bill:billDb) {
					if(bill.getDate().equals(todayDate)) {
						todayBillList.add(bill);
					}
				}
				return todayBillList;

			}
			else {
				throw new UserException("Database is empty, customer did not purchased");
			}

		}
		//returns this month bills
		public synchronized List<Bill> getThisMonthBill() throws UserException{
			if(!billDb.isEmpty()) {
				List<Bill> thisMonthBillList=new LinkedList<>();
				for(Bill bill:billDb) {
					if(bill.getDate().getMonthValue()==LocalDate.now().getMonthValue()) {
						thisMonthBillList.add(bill);
					}
				}
				return thisMonthBillList;
			}
			else {
				throw new UserException("Database is empty, customer did not purchased");
			}
		}
		//returns all bills
		public synchronized List<Bill> getAllBills() throws UserException{
			if(!billDb.isEmpty()) {
				return billDb;
			}
			else {
				throw new UserException("database is empty,no customer is purchased");
			}
		}
}
