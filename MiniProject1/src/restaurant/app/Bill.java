package restaurant.app;

import java.time.LocalDate;
import java.util.Map;
import java.util.Map.Entry;


public class Bill {
	private String customerName;
	private Map<Integer, Item> bill;
	private LocalDate date= LocalDate.now();
	public Bill(String customerName, Map<Integer, Item> bill) {
		super();
		this.customerName = customerName;
		this.bill = bill;
	}
	//return map of purchased items
	public Map<Integer, Item> getPurchasedItem(){
		return bill;
	}
	//return total bill
	public int getTotalBill() {
		int total=0;
		for(Entry<Integer, Item>item:bill.entrySet()) {
			total+=item.getValue().getItemPrice();

		}
		return total;
	}
	// return the time stamp
	public LocalDate getDate() {
		return date;
	}
	//return customer name
	public String getCustomerName() {
		return customerName;
	}
}
