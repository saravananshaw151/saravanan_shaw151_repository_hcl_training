package restaurant.app;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
public class User extends Thread {
         
	private String userName;

	public User(String userName) {
		super();
		this.userName = userName;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Scanner sc= new Scanner(System.in);
		// creating a map to store item purchased by customer
		Map<Integer, Item> mapOfItemPurchased = new LinkedHashMap<>();
		try {
			Map<Integer, Item> itemDatabase= new ItemDatabase().getItemDb();
			int press;
			do {
				press=0;
				//displaying the menu
				System.out.println("Todays Menu :-");
				for(Map.Entry<Integer, Item>item: itemDatabase.entrySet()) {
					System.out.println(item.getKey()+":"+item.getValue());
				}
				//getting item code
				System.out.println("\n enter the menu item code");
				int itemCode= sc.nextInt();
				//adding the item in purchased map
				mapOfItemPurchased.put(itemCode, itemDatabase.get(itemCode));
				System.out.println("To order more, press 0\n to finish order, press other than 0");
				press=sc.nextInt();
				
				
			}while(press==0);
			//displaying the receipt
			System.out.println("\n Thanks "+userName+" for dining in with surabi\n");
			System.out.println("Your receipt");
			for(Entry<Integer, Item>item:mapOfItemPurchased.entrySet()) {
				System.out.println(item.getKey()+":"+item.getValue());
			}
			Bill bill = new Bill(userName, mapOfItemPurchased);
			System.out.println("\n Your total bill will be:- "+bill.getTotalBill()+"\n You logged out successful\n");
			//adding the bill to bill database with time stamp
			BillDatabase.addBill(bill);
			
		}catch(UserException e) {
			System.out.println(e.getMessage());
		}
		
		
	}
	
}
