package restaurant.app;


import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;

public class Admin extends Thread {
 
	@Override
	public void run() {
		BillDatabase n = new BillDatabase();
		//displaying the choices
		System.out.println("\n to Display today bill, enter 1");
		System.out.println("\n to Display this month bill, enter 2");
		System.out.println("\n to Display all bill, enter 3\n");
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the choice");
		int choice =sc.nextInt();
		
		try {
			switch(choice) {
			case 1:
				//to display today bill
				display(n.getTodayBill());
				break;
			case 2:
				//to display the this month bill
				display(n.getThisMonthBill());
				break;
			case 3:
				//to display all bills
				display(n.getAllBills());
				break;
			default:
				System.out.println("wrong choice");

			}
		}catch(UserException e) {
			System.out.println(e.getMessage()+"\n");
		}
		System.out.println("\n you logged out successfully");
	}
	//display function
	public void display(List<Bill>bills) {
		for(Bill b:bills) {
			System.out.println("username: "+b.getCustomerName());
			System.out.print("Items:-[");
			for(Entry<Integer, Item>item:b.getPurchasedItem().entrySet()) {
				System.out.println(item.getKey()+" "+item.getValue()+",");
			}
			System.out.print("]\n");
			System.out.println("total bill:- "+b.getTotalBill());
			System.out.println("Date:"+b.getDate()+"\n");
		}
	}


	
	
}
