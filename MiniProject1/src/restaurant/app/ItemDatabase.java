package restaurant.app;

import java.util.LinkedHashMap;
import java.util.Map;
public class ItemDatabase {
	//item database
		private static Map<Integer, Item> itemDb= new LinkedHashMap<>();
		
		//returning the item database
		public synchronized Map<Integer, Item> getItemDb() throws UserException{
			if(itemDb.isEmpty()) {
				throw new UserException("Database is empty");
			}
			return itemDb;
			
		}
		//to add item to item database
		public static synchronized void addItem(Integer itemId, Item item) {
			itemDb.put(itemId, item);
		}
}
