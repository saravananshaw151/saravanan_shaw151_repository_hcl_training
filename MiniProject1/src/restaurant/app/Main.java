package restaurant.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;


public class Main {

	public static void main(String[] args) throws Exception{
		
		Scanner sc = new Scanner(System.in);
		//creating a file
		File loginDetails = new File("loginDetails.txt");
		if(!loginDetails.exists()) {
			if(loginDetails.createNewFile()) {
				System.out.println("Login details file created");
			}
		}
		//updating the login details file with name login details
		FileWriter f=new FileWriter(loginDetails);
		f.write("viay 5656\n");
		f.write("logesh 7878\n");
		f.write("ram 1212\n");
		f.close();
		
		// adding item menu
		ItemDatabase.addItem(1, new Item("Rajma chaval",2,150.0));
		ItemDatabase.addItem(2, new Item("Momos",2,190.0));
		ItemDatabase.addItem(3, new Item("Red thai curry",2,180.0));
		ItemDatabase.addItem(4, new Item("chaap",2,190.0));
		ItemDatabase.addItem(5, new Item("chilly potato",1,250.0));

		System.out.println("Welcome to Surabi resturant\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		while(true) {
			//Entering the login details
			System.out.println("Please enter your credentials");
			System.out.println("Email id= ");
			String userEmailId=sc.next();
			System.out.println("Password= ");
			String userPassword= sc.next();
			//verifying the credentails with login details file
			if(!isValid(userEmailId, userPassword, loginDetails)) {
				System.out.println("Please enter the valid user id and password");
				continue;
			}
			else {
				System.out.println("Please enter A if you are admin, U if you are User");
				char user=sc.next().charAt(0);
				switch(user) {
				case 'A':
					System.out.println("You logged in as admin successfully\n weclome "+userEmailId+"\n");
					Admin adm= new Admin();
					adm.start();
					try {
						adm.join();
					}catch(InterruptedException e) {
						e.printStackTrace();
					}
					break;
				case 'U':
					System.out.println("You logged in as user successfull\n welcome "+userEmailId);
					User user1=new User(userEmailId);
					user1.start();
					try {
						user1.join();
						
					}catch(InterruptedException e) {
						e.printStackTrace();
					}
					break;
					
				}
			}
			int flag;
			System.out.println("To continue enter 0 or To terminate enter other than 0");
			flag=sc.nextInt();
			if(flag!=0) {
				System.exit(0);
			}
			
		}
		
	}
	
	static boolean isValid(String userEmailId, String userPassword, File loginDetails) throws IOException{
		FileReader fr= null;
		BufferedReader br=null;
		try {
			fr=new FileReader(loginDetails);
			br= new BufferedReader(fr);
			String l;
			while((l=br.readLine())!=null) {
				String[] value=l.split(" ");
				if(value[0].equalsIgnoreCase(userEmailId)){
					if(value[1].equals(userPassword)) {
						return true;
					}
				}
			}
		}catch(FileNotFoundException e) {
			System.out.println("LoginDetails files is not found");

		}finally {
			fr.close();
			br.close();
		}
		return false;

		
	}
	

}
