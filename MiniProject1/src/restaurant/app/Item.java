package restaurant.app;

public class Item {
	private String itemName;
	private int quantity;
	private double itemPrice;
	//constructor
	public Item(String itemName, int quantity, double itemPrice) {
		super();
		this.itemName = itemName;
		this.quantity = quantity;
		this.itemPrice = itemPrice;
	}
	//getters and setters
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}
	@Override
	public String toString() {
		return "Item [itemName=" + itemName + ", quantity=" + quantity + ", itemPrice=" + itemPrice + "]";
	}
	
	
}
