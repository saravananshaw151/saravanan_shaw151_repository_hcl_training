package week1assignment;

public class MainClass {

	public static void main(String[] args) {
		AdminDepartment ad = new AdminDepartment();
		HrDepartment hr = new HrDepartment();
		TechDepartment tc = new TechDepartment();
		SuperDepartment sp = new SuperDepartment();
		
		System.out.println("Super Department functionalities");
		System.out.println("Dept Name  -"+sp.departmentName());
		System.out.println("Todays work  -"+sp.getTodaysWork());
		System.out.println("Work deadline  -"+sp.workDeadline());
		System.out.println("ABout holiday  -"+sp.isTodayHoliday() );
		System.out.println();
		
		System.out.println("AdminDepartment functionalities");
		System.out.println("Dept Name  -"+ad.departmentName());
		System.out.println("Todays work  -"+ad.getTodaysWork());
		System.out.println("Work deadline  -"+ad.workDeadline());
		System.out.println("About Holiday  -"+ad.isTodayHoliday());
		System.out.println();
		System.out.println("Hr Department functionalities");
		System.out.println("Dept Name  -"+hr.departmentName());
		System.out.println("Todays work  -"+hr.getTodaysWork());
		System.out.println("Work deadline  -"+hr.workDeadline());
		System.out.println("About Holiday   -"+hr.isTodayHoliday());
		System.out.println("Activity  -"+hr.activity());
		System.out.println();
		System.out.println("TechDepartment functionalities");
		System.out.println("Dept Name  -"+tc.departmentName());
		System.out.println("Todays work  -"+tc.getTodaysWork());
		System.out.println("Work deadline  -"+tc.workDeadline());
		System.out.println("About Holiday  -"+tc.isTodayHoliday());
		System.out.println("Stack Information  -"+tc.techStackInformation());
	}

}
