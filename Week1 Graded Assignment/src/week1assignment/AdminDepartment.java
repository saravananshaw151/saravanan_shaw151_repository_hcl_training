package week1assignment;

public class AdminDepartment extends SuperDepartment {
   
	public String departmentName(){
		return "Admin Department";
	}
	public String getTodaysWork() {
		return "Complete your documents Submission";
	}
	public String workDeadline() {
		return "Complete by End of the Day";
	}
	
}
