package week1assignment;

public class HrDepartment extends SuperDepartment{
	public String departmentName(){
		return "HR Department";
	}
	public String getTodaysWork() {
		return "Fill todays worksheet and mark your attendance";
	}
	public String workDeadline() {
		return "Complete by End of the day";
	}
	public String activity() {
		return "Team Lunch";
	}
}
