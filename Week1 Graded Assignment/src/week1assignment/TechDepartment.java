package week1assignment;

public class TechDepartment extends SuperDepartment{
    
	
	public String departmentName(){
		return "Tech Department";
	}
	public String getTodaysWork() {
		return "Complete Coding of Module 1";
	}
	public String workDeadline() {
		return "Complete by ENd of the Day";
	}
	public String techStackInformation() {
		return "Core JAVA";
	}
}
