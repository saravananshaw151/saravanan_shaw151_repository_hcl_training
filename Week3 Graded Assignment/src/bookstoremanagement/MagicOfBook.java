package bookstoremanagement;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MagicOfBook {


	
	
	public void Add(HashMap<Integer, BookDetails> hm,int k,String bname,double bprice,String bgenre,int bnocopy){
		
		  boolean f=true;
		  for(Object i:hm.keySet()) {
			  if(hm.containsKey(k)) {
				  f=false;
				  break;
			  }
		  }
		  if(f==false) {
			  System.out.println("Book Id must be unique");
		  }
		  else {
			  
			  hm.put(k, new BookDetails(bname, bprice, bgenre, bnocopy));    
				System.out.println("Details Added Successfully");
		  }
	}
	public void Update(HashMap<Integer, BookDetails> hm,int k,String bname,double bprice,String bgenre,int bnocopy) {
		hm.replace(k, new BookDetails(bname, bprice, bgenre, bnocopy));
		System.out.println("Updated successfully");
		
	}
	public void delete(HashMap<Integer, BookDetails> hm,int f) {
		if(hm.containsKey(f)) {
			hm.remove(f);
		}
		else {
			System.out.println("Entered ID is invalid");
		}
		System.out.println("Deleted successfully");
	}
	
	public void display(HashMap<Integer, BookDetails> hm) {
		Set map = hm.entrySet();
	       Iterator it = map.iterator();
	       while(it.hasNext()) {
	    	   System.out.println((Map.Entry)it.next());
	       }
	}
	public void countTotalNumberOfBooks(HashMap<Integer, BookDetails> hm) {
		int count=0;
		for(Object i :hm.keySet()) {
			count=count+hm.get(i).getNoOfCopiesSold();
		}
		System.out.println("Total Number of Books are - "+count);
	}
	public void underAutobiographyGenre(HashMap<Integer, BookDetails> hm) {
		System.out.println("Books Under Autobiography Genre are:");
		boolean f = true;
		for(Object i:hm.keySet()) {
			if((hm.get(i).getGenre()).equals("Autobiography")) {
				f = false;
				System.out.println(hm.get(i).getName());
			}
		}
		if(f==true) {
			System.out.println("None of the books Under Autobiography genre");
		}
		
	}
}
