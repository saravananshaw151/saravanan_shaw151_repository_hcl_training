package bookstoremanagement;

import java.util.Comparator;

public class PriceSortingHighToLow implements Comparator<BookDetails>{

	@Override
	public int compare(BookDetails o1, BookDetails o2){
		
		return (int)(o2.getPrice()-o1.getPrice());
	}
    
}
