package bookstoremanagement;

public class BookDetails {

	private String Bookname;
	private double price;
	private String genre;
	private int noOfCopiesSold;
	public BookDetails() {
		super();
		
	}
	public BookDetails(String Bookname, double price, String genre, int noOfCopiesSold) {
		super();
		this.Bookname = Bookname;
		this.price = price;
		this.genre = genre;
		this.noOfCopiesSold = noOfCopiesSold;
	}
	public String getName() {
		return Bookname;
	}
	public void setName(String name) {
		this.Bookname = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public int getNoOfCopiesSold() {
		return noOfCopiesSold;
	}
	public void setNoOfCopiesSold(int noOfCopiesSold) {
		this.noOfCopiesSold = noOfCopiesSold;
	}
	@Override
	public String toString() {
		return "BookDetails [Bookname=" + Bookname + ", price=" + price + ", genre=" + genre + ", noOfCopiesSold="
				+ noOfCopiesSold + "]";
	}
	
	
	
	
}
