package bookstoremanagement;

import java.util.Comparator;

public class PriceSortingLowToHigh implements Comparator<BookDetails>{

	@Override
	public int compare(BookDetails o1, BookDetails o2) {
		
		return (int)(o1.getPrice()-o2.getPrice());
	}

}
