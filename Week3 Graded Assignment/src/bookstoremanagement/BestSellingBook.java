package bookstoremanagement;

import java.util.Comparator;

public class BestSellingBook implements Comparator<BookDetails> {

	@Override
	public int compare(BookDetails o1, BookDetails o2) {
		
		return o2.getNoOfCopiesSold()-o1.getNoOfCopiesSold();
	}

}
