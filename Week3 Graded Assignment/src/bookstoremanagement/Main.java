package bookstoremanagement;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		
		HashMap<Integer, BookDetails> mp = new HashMap<>();
		
        Scanner in = new Scanner(System.in);
        int n;String st ;
        try {
		do {
			System.out.println("Enter 1 to add the Books");
			System.out.println("Enter 2 to update the Books");
			System.out.println("Enter 3 to delete the Books");
			System.out.println("Enter 4 to display the Books");
			System.out.println("Enter 5 to display total number of books");
			System.out.println("Enter 6 to display the Books under Autobiography genre");
			System.out.println("Enter 7 to do Operations like price wise sort and Best selling Book");
			MagicOfBook mg = new MagicOfBook();
			
			n = in.nextInt();
			switch(n) {
			case 1:    
				       String bookName; double bookPrice; String bookgenre;int bookNoOfCopy;
				       int key;
				       
				       System.out.println("Enter the Book ID ");
                       key = in.nextInt();
                       System.out.println("Enter the name of book without space"); 
                       bookName=in.next();
				       System.out.println("Enter the price of book"); 
				       bookPrice = in.nextDouble();
				       System.out.println("Enter the genre of book"); 
				       bookgenre = in.next();
				       System.out.println("Enter the number of copies sold");
				       bookNoOfCopy = in.nextInt();
				       mg.Add(mp,key, bookName, bookPrice, bookgenre, bookNoOfCopy);
				       
				      
				       break;
			case 2:   
				       String bkName; double bkPrice; String bkgenre;int bkNoOfCopy;
				       int k;
				       System.out.println("Enter the Book ID you want to Update");
				       k = in.nextInt();
				       System.out.println("Enter the name of book without space"); 
                       bkName = in.next();
				       System.out.println("Enter the price of book"); 
				       bkPrice = in.nextDouble();
				       System.out.println("Enter the genre of book"); 
				       bkgenre = in.next();
				       System.out.println("Enter the number of copies sold");
				       bkNoOfCopy = in.nextInt();
				       mg.Update(mp,k,bkName,bkPrice,bkgenre,bkNoOfCopy);
				       break;
				       
			case 3:  
				       int j;
				       System.out.println("Enter the ID of Book you want to delete");
				       j = in.nextInt();
				       mg.delete(mp,j);
				       
				       break;
				       
			case 4:    
				       System.out.println("Book details:");
				       mg.display(mp);
				       
				       break;
			case 5 :
				      mg.countTotalNumberOfBooks(mp);
				      break;
			case 6:   
				      mg.underAutobiographyGenre(mp);
				      break;
			case 7:
				    int num;
				    System.out.println("Enter 1 to print Books ascending sort price wise ");
				    System.out.println("Enter 2 to print Books descending sort price wise");
				    System.out.println("Enter 3 to print the best selling Books order wise");
				    num = in.nextInt();
				    switch(num) {
				    case 1: 
				    	    Collection<BookDetails> bd = mp.values();
				    	    
				            List list = new ArrayList(bd);
				            Collections.sort(list, new PriceSortingLowToHigh());
				            System.out.println("Pricewise Sorting in Low to High");
				    		for(Object bk:list) {
				    			System.out.println(bk);
				    		}
				    	    break;
				    case 2:  
				    	    Collection<BookDetails> bkd = mp.values();
				    	    List li = new ArrayList(bkd);
				            Collections.sort(li, new PriceSortingHighToLow());
				            System.out.println("Pricewise Sorting in High to Low");
				    		for(Object bk:li) {
				    			System.out.println(bk);
				    		}
				    	    break;
				    case 3: 
				    	 Collection<BookDetails> b = mp.values();
				    	    List l = new ArrayList(b);
				            Collections.sort(l, new BestSellingBook());
				            System.out.println("Best seling Books Top to down order");
				    		for(Object bk:l) {
				    			System.out.println(bk);
				    		}
				    	    
				    	    break;
				    default:
				    	  System.out.println("You Entered Invalid option");
				    }
				    break;
				      
		    default:
		    	     System.out.println("Enter the correct details as per the conditions");
			}
			
			
			System.out.println("Do you want to continue just type Y or N");
			st = in.next();
			
			}
		while(st.equals("Y"));
		
		System.out.println("Operations done");
		}
		 catch(Exception e) {
	    	   System.out.println("Please enter the data according to the data type  -"+e);
	       }
	      in.close();

	}

}
