package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com")
@EnableJpaRepositories(basePackages = "com.dao")
@EntityScan(basePackages = "com.bean")
public class MiniProject2withSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniProject2withSpringBootApplication.class, args);
		System.out.println("Server running on port number 9090");
	}

}
