package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bean.UserEntity;

@Repository
public interface UserInterface extends JpaRepository<UserEntity, String> {
	
	public UserEntity findByUsernameAndPassword(@Param("username") String username,@Param("password")String password);
	
}
