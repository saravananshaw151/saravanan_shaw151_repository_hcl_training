package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bean.Admincredentials;

@Repository
public interface AdminDao extends JpaRepository<Admincredentials, String>{

	public Admincredentials findByEmailAndPassword(@Param("userId") String email,@Param("pass") String password);
}
