package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.bean.Admincredentials;
import com.bean.UserEntity;
import com.service.AdminService;
import com.service.UserService;

@Controller

public class AdminController {

	@Autowired
	AdminService adminService;
	
	@Autowired
	UserService userService;
	

	
	@RequestMapping(value="/adminlogin",method=RequestMethod.POST)
	public ModelAndView loginAdmin(@RequestParam("userId") String UserId,@RequestParam("pass") String password) {
		
		ModelAndView mav = new ModelAndView();
		Admincredentials admin = new Admincredentials();
	    admin.setEmail(UserId);admin.setPassword(password);
	    
		String msg = adminService.adminLogin(admin);
		if(msg.startsWith("Welcome")) {
		  mav.addObject("info",msg );
		  mav.setViewName("adminHome.jsp");
		}
		else {
			mav.addObject("info", msg);
			mav.setViewName("adminlogin.jsp");
		}
		return mav;
	}
	
	@RequestMapping(value="/adminregister",method= RequestMethod.POST)
	public ModelAndView registerAdmin(@RequestParam("userId") String emailid,@RequestParam("pass") String password){
		ModelAndView mav = new ModelAndView();
		Admincredentials adm = new Admincredentials();
		adm.setEmail(emailid);
		adm.setPassword(password);
		mav.addObject("msg", adminService.adminRegister(adm));
		mav.setViewName("adminregister.jsp");
		return mav;
	}
	
	@RequestMapping(value="/adminlogout")
	public ModelAndView adminLogout() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index.jsp");
		return mav;
	}
	
	@GetMapping(value="getuser")
	public ModelAndView getAllUsers(HttpSession hs) {
		ModelAndView mav =  new ModelAndView();
		List<UserEntity> userinfo = userService.getAllUsers();
		hs.setAttribute("user", userinfo);
		mav.setViewName("userCRUD.jsp");
		return mav;
	}
	
	@RequestMapping(value = "deleteUser",method = RequestMethod.GET)
	public ModelAndView deleteUser(HttpServletRequest req,HttpSession hs) {			
		String id = req.getParameter("userid");
		String res = userService.deleteUser(id);
		List<UserEntity> listOfUser = userService.getAllUsers();
		hs.setAttribute("user", listOfUser);		
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("message", res);
		mav.setViewName("userCRUD.jsp");
		return mav;
	}
	
	@RequestMapping(value = "updateUser",method=RequestMethod.GET)
	public ModelAndView updateUser(HttpServletRequest req,HttpSession hs) {			
		String id = req.getParameter("userid");
		String password = userService.getUserPassword(id);
		hs.setAttribute("emailid", id);
		hs.setAttribute("pass", password);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("updateUserdetails.jsp");
		return mav;
	}
	
	@RequestMapping(value="/updateUserRecord",method=RequestMethod.POST)
	public ModelAndView updateSuccessMsg(@RequestParam("userid") String userid,@RequestParam("secret") String pass,@RequestParam("address") String address,@RequestParam("contact") long contact,@RequestParam("city") String city,@RequestParam("state") String State) {
UserEntity user = new UserEntity();
		user.setUsername(userid);
		user.setPassword(pass);
		user.setAddress(address);
		user.setContact(contact);
		user.setCity(city);
		user.setState(State);
		ModelAndView mav = new ModelAndView();
		mav.addObject("msg", userService.updateUser(user));
		mav.setViewName("updateUserdetails.jsp");
		return mav;
	}
	
	
	
	
}
