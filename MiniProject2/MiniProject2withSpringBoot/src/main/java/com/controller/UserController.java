package com.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.bean.UserEntity;
import com.service.UserService;

@Controller
public class UserController {

	@Autowired
	UserService userService;
	
	@GetMapping(value="/")
	public ModelAndView indexpage() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index.jsp");
		return mav;
	}

	
	@RequestMapping(value="/loginuser",method=RequestMethod.POST)
	public ModelAndView loginUser(@RequestParam("user") String UserId,@RequestParam("pass") String password) {
		
		ModelAndView mav = new ModelAndView();
		UserEntity userinfo = new UserEntity();
		userinfo.setUsername(UserId);userinfo.setPassword(password);
		String msg = userService.signIn(userinfo);
		if(msg.startsWith("Welcome")) {
		  mav.addObject("info",msg );
		  mav.setViewName("userHome.jsp");
		}
		else {
			mav.addObject("info", msg);
			mav.setViewName("userlogin.jsp");
		}
		return mav;
	}
	
	@RequestMapping(value="/registeruser",method= RequestMethod.POST)
	public ModelAndView registerUser(@RequestParam("user") String userid,@RequestParam("pass") String password,@RequestParam("address") String resident,@RequestParam("contact") long phone,@RequestParam("city") String cityname,@RequestParam("state") String Statename) {
		ModelAndView mav = new ModelAndView();
		UserEntity userdetail = new UserEntity();
		userdetail.setUsername(userid);
		userdetail.setPassword(password);
		userdetail.setAddress(resident);
		userdetail.setContact(phone);
		userdetail.setCity(cityname);
		userdetail.setState(Statename);
		
		mav.addObject("msg", userService.signUp(userdetail));
		mav.setViewName("userregister.jsp");
		return mav;
	}
	
	@RequestMapping(value="/userlogout")
	public ModelAndView userLogout() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index.jsp");
		return mav;
	}
	
}
