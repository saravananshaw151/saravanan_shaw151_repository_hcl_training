package com.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.UserEntity;
import com.dao.UserInterface;

@Service
public class UserService {

	@Autowired
	UserInterface userDao;
	
	public String signIn(UserEntity user) {
		UserEntity us = userDao.findByUsernameAndPassword(user.getUsername(), user.getPassword());
		if(Objects.nonNull(us)) {
			return "Welcome User, "+user.getUsername()+" You are Successfully logged IN. Order Some delicious food today";
		}
		else {
			return "Invalid Username or password";
		}
	}
	
	public String signUp(UserEntity user) {
		UserEntity user1 = userDao.findByUsernameAndPassword(user.getUsername(),user.getPassword());
		if(Objects.nonNull(user1)) {
			return "User Already Exists try with different email";
		}
		else {
			userDao.save(user);
			
			return "User Registered Successfuly, Now you can proceed to login";
		}
	}
	
	public List<UserEntity> getAllUsers(){
		return userDao.findAll();
	}
	
	public String deleteUser(String userId) {
		if(!userDao.existsById(userId)) {
			return "User Not Exists in this ID";
		}
		else {
			userDao.deleteById(userId);
			return "User Deleted Successfully";
		}
	}
	public String getUserPassword(String id) {
		
		return userDao.getById(id).getPassword();
	}
	
	public String updateUser(UserEntity user) {
		if(!userDao.existsById(user.getUsername())) {
			return "User not exists in this ID";
		}
		else {
			userDao.save(user);
			return "User details Updated Successfully";
		}
	}
}
