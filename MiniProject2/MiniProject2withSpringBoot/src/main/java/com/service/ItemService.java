package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.ItemsDetails;
import com.dao.ItemDao;

@Service
public class ItemService {

	@Autowired
	ItemDao itemDao;
	
	public  String createNewItem(ItemsDetails item) {
		if(itemDao.existsById(item.getItemid())) {
			return "Item Already present in this ID";
		}
		else {
			itemDao.save(item);
			return "Item created Successfully";
		}
	}
	
	public List<ItemsDetails> getAllItems(){
	    return itemDao.findAll();	
	}

	public String deleteItem(int itemId) {
		if(!itemDao.existsById(itemId)) {
			return "Item Not Exists in this ID";
		}
		else {
			itemDao.deleteById(itemId);
			return "Item Deleted Successfully";
		}
	}
	
	public String updateItem(ItemsDetails item) {
		if(!itemDao.existsById(item.getItemid())) {
		  
			return "Item Not exists in this ID";
		}
		else {
			 itemDao.saveAndFlush(item);
			return "Item Information Updated Successfully";
		}
	}
	
	
}
