package com.service;


import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Admincredentials;

import com.dao.AdminDao;


@Service
public class AdminService {

	@Autowired
	AdminDao adminDao;
	
	
	
	public String adminLogin(Admincredentials admin) {
		
		Admincredentials adm = adminDao.findByEmailAndPassword(admin.getEmail(), admin.getPassword());
		if(Objects.nonNull(adm)) {
			return "Welcome Admin "+ admin.getEmail()+" You are logged in successfully";
		}
		else {
			return "Invalid credentials, try again";
		}
	}
	
	
	public String adminRegister(Admincredentials admin) {
		Admincredentials adm1 = adminDao.findByEmailAndPassword(admin.getEmail(),admin.getPassword());
		if(Objects.nonNull(adm1)) {
			return "Admin Details Already Exists,Try with different Id";
		}
		else {
			adminDao.save(admin);
			return "You have Registered as admin and You can proceed to login";
		}
	}
	

}
