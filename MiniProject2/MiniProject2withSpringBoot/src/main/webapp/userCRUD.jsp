<%@page import="java.util.Iterator"%>
<%@page import="com.bean.UserEntity"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2 align="center">Welcome to SURABI Restaurant (Admin usage)</h2><br/><hr/>
<h3>All Users Information</h3><br/>
Note: Retrieve done when you click the link<br/>
<h4>Create New User <a href="userregister.jsp">Click here</a></h4>
<br/>
<div align="center">
<table border="3">
	<tr>
		<th>EmailId</th>
		<th>Address</th>
		<th>contact</th>
		<th>City</th>
		<th>State</th>
		<th>Delete</th>
		<th>Update</th>
	</tr>
<%
Object obj = session.getAttribute("user");
List<UserEntity> listOfuser = (List<UserEntity>)obj;
Iterator<UserEntity> li = listOfuser.iterator();
while(li.hasNext()){
	UserEntity user = li.next();
	%>
	<tr>
		<td> <%=user.getUsername() %></td>
		<td> <%=user.getAddress() %></td>
		<td> <%=user.getContact() %></td>
		<td> <%=user.getCity() %></td>
		<td> <%=user.getState() %></td>
		<td><a href="deleteUser?userid=<%=user.getUsername() %>">Delete Record</a> </td>
		<td><a href="updateUser?userid=<%=user.getUsername() %>">Update Record</a> </td>
	</tr>
	<%
}
%>
</table>
</div><br/>
 ${message}
<br/><br/><br/>
<div align="center">
   <a href="adminHome.jsp">Get Back to Admin Home Page</a>
</div>
</body>
</html>