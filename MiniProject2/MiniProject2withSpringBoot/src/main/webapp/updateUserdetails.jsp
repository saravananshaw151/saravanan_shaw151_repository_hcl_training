<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Update User Record</h2>
<form action="updateUserRecord" method="post">
<label>Email Id : </label>
<input type="email" name="userid" value="<%=session.getAttribute("emailid")%>" readonly="readonly"><br/>
<label>Password : </label>
<input type="password" name="secret" value="<%=session.getAttribute("pass")%>" readonly="readonly" ><br/>
<label>Address : </label>
<input type="text" name="address"><br/>
<label>Contact : </label>
<input type="tel" name="contact" /><br/>
<label>City : </label>
<input type="text" name="city" /><br/>
<label>State : </label>
<input type="text" name="state"/><br/>
<input type="submit" value="UPDATE">&nbsp;<input type="reset" value="RESET">
</form>
<br/>
${msg}
<br/>
<div align="center">
 <form action="getuser">
 <input type ="submit" value="Get Back to User CRUD Page" />
 </form>
</div>
</body>
</html>