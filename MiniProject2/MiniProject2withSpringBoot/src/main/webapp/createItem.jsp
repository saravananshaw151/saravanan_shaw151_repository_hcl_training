<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Create New Item</title>
</head>
<body>
<h2 align="center">Welcome To Surabi-Restaurant (Admin Usage)</h2><hr/><br/>

<h3>Create Your new Item Here</h3><br/>

<form action="createNewItem" method="post">
  <label>Item Id    : </label>
  <input type="number" name="id" /><br/>
  <label>Item Name  : </label>
  <input type="text" name="itname"/><br/>
  <label>Item Image : </label>
  <input type="text" name="iturl" /><br/>
  <label>Item Price : </label>
  <input type="number" name="itprice"/><br/>
  <input type="submit" value="Create Item" /> &nbsp;
  <input type="reset" value="reset"/>
</form>
<br/>
${messg}

<br/>
<div align="center">
 <form action="menuCRUD">
 <input type ="submit" value="Get Back to Item CRUD Page" />
 </form>
</div>
</body>
</html>