<%@page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book Store Login</title>

<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script type="text/javascript"
    src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
    
<style>
* {
  box-sizing: border-box;
}

.row {
  display: flex;
}

/* Create three equal columns that sits next to each other */
.column {
  flex: 33.33%;
  padding: 5px;
}
</style>
    
</head>
<body>

 <div style="text-align: center">
        <h1>User Login</h1>
        <form action="LoginController" method="post">
            <label>username</label>
            <input type="email" name="email"/>
            <br/>
            <label>password</label>
            <input type="password" name="password"/>
            <br>${message}
            <br/><br/>         
            <input type="submit" value="Login"/>
            <input type="reset" value="reset"/>
        </form>
    </div>
<script type="text/javascript">
 
    $(document).ready(function() {
        $("#loginForm").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
         
                password: "required",
            },
             
            messages: {
                email: {
                    required: "Please enter email",
                    email: "Please enter a valid email address"
                },
                 
                password: "Please enter password"
            }
        });
 
    });
</script>
<h2 align="center">List Of Books Available</h2>
<div class="row">
  <div class="column">
    <img src="https://m.media-amazon.com/images/I/51Vbnl7JReL.jpg" alt="book1" width="180px" height="240px">
    <h2>Title: Fairy Tales</h2>
  </div>
  <div class="column">
    <img src="https://www.notimeforflashcards.com/wp-content/uploads/2019/07/diverse-fairytales-little-mermaid--455x489.jpg" alt="book2" width="180px" height="240px">
    <h2>Title: The Little Mermaid</h2>
  </div>
  <div class="column">
    <img src="https://images-na.ssl-images-amazon.com/images/I/81CZR4I2R4L.jpg" alt="book3" width="180px" height="240px">
    <h2>Title: The PRINCES & PEA</h2>
  </div>
  
  <div class = "column">
  <img src="https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Harry-Potter-and-the-Chamber-of-Secrets-book-cover.jpg" alt="book4" width="180px" height = "240px">
  <h2>Title: Harry Potter & chamber of secrets</h2>
  </div>
   <div class = "column">
  <img src="https://img1.exportersindia.com/product_images/bc-full/dir_136/4065051/best-of-series-story-books-p-b-bw-1514537556-3549957.jpeg" alt="book5" width="180px" height = "240px">
  <h2>Title: Arabian Nights</h2>
  </div>
  
</div>

</body>
</html>