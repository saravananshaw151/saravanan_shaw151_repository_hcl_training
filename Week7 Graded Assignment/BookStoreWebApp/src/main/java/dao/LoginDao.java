package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import bean.LoginDetails;
import db.connection.DbResource;

public class LoginDao {

	public int registerUser(LoginDetails login) {
		
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pst = con.prepareStatement("insert into logincredentials values(?,?)");
			pst.setString(1, login.getUsername());
			pst.setString(2, login.getPassword());
			
		   return pst.executeUpdate();	
		}
		catch(Exception e) {
			System.out.println("register method error"+e);
			return 0;
		}
		
	}
	public LoginDetails checkLogin(LoginDetails login) {
		try {
			Connection con1=DbResource.getDbConnection();
			PreparedStatement pst = con1.prepareStatement("select *from logincredentials where username=? and password=?");
			pst.setString(1, login.getUsername());
			pst.setString(2, login.getPassword());

	        ResultSet result = pst.executeQuery();
	 
	      if(result.next()) {
	    	  login.setUsername(login.getUsername());
	      }
	 
		}
		catch(Exception e) {
			System.out.println("checkLogin method "+e);
		}
		return login;
	}
	
	
	
	
}
