<%@page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book Store Application</title>
<style>
* {
  box-sizing: border-box;
}

.row {
  display: flex;
}

/* Create three equal columns that sits next to each other */
.column {
  flex: 33.33%;
  padding: 5px;
}
</style>

</head>
<body>
<h1 align ="center">Book Store Application</h1>
<hr/><br/>
<div align="center">
<font size="05"><a href="login">Existing User CLick Here to Login</a></font>
</div><br/>

<p align="center">Note: Below credentials should apply for New User Registeration</p>
<div align="center">
<form action="RegisterController" method="post">
 <label>Username : </label>
 <input type="text" name = "user"/><br/><br/>
 <label>Password : </label>
 <input type="password" name ="pass"/><br/><br/>
 <input type="submit" value ="Register"/>
 <input type="reset" value="reset"/>
</form>
</div>


<br/>
<br/>
<br/>
<br/>

<h2 align="center">List Of Books Available</h2>
<div class="row">
  <div class="column">
    <img src="https://m.media-amazon.com/images/I/51Vbnl7JReL.jpg" alt="book1" width="180px" height="240px">
    <h2>Title: Fairy Tales</h2>
  </div>
  <div class="column">
    <img src="https://www.notimeforflashcards.com/wp-content/uploads/2019/07/diverse-fairytales-little-mermaid--455x489.jpg" alt="book2" width="180px" height="240px">
    <h2>Title: The Little Mermaid</h2>
  </div>
  <div class="column">
    <img src="https://images-na.ssl-images-amazon.com/images/I/81CZR4I2R4L.jpg" alt="book3" width="180px" height="240px">
    <h2>Title: The PRINCES & PEA</h2>
  </div>
  
  <div class = "column">
  <img src="https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Harry-Potter-and-the-Chamber-of-Secrets-book-cover.jpg" alt="book4" width="180px" height = "240px">
  <h2>Title: Harry Potter & chamber of secrets</h2>
  </div>
   <div class = "column">
  <img src="https://img1.exportersindia.com/product_images/bc-full/dir_136/4065051/best-of-series-story-books-p-b-bw-1514537556-3549957.jpeg" alt="book5" width="180px" height = "240px">
  <h2>Title: Arabian Nights</h2>
  </div>
  
</div>


</body>
</html>