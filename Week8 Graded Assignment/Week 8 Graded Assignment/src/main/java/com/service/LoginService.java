package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.BookDetails;
import com.bean.LoginCredentials;
import com.dao.LoginDao;
@Service
public class LoginService {

	@Autowired
	LoginDao loginDao;
	
	public String RegiterUser(LoginCredentials user) {
		if(!user.getUsername().endsWith("@gmail.com")) {
			return "Sorry!, Please enter proper Username (UserId Should ends with @gmail.com)";
		}
		else {
			if(loginDao.registerUser(user)>0) {
				return "You Have registered Successfully, Now you can proceed to login";
			}
			else {
				return "Didn't Register the credentials Or User has been Already registered";
			}
		}
	}
	
	public String LoginUser(LoginCredentials user) {
		
		if(loginDao.loginUser(user)!=null) {
			return "Welcome User, "+loginDao.loginUser(user)+". You have Logged in successfully";
		}
		else {
			return "Invalid User credentials, because of field may empty or mismatch of userId and password";
		}
	}
	
	public List<BookDetails> getAllBooks(){
		return loginDao.getAllBooks();
	}
	
	
}
