package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bean.BookDetails;
import com.bean.LoginCredentials;
@Repository
public class LoginDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public int registerUser(LoginCredentials user) {
		try {
			int res = jdbcTemplate.update("insert into logincredentials values(?,?)", new Object[] {user.getUsername(),user.getPassword()});
			return res;
		}
		catch(Exception e) {
			System.out.println("register User method "+e);
			return 0;
		}
	}
	
	public String loginUser(LoginCredentials user) {
		try {

			String userId = jdbcTemplate.queryForObject("select username from logincredentials where username=? and password=?", new Object[] {
					user.getUsername(), user.getPassword() }, String.class);

			return userId.substring(0, userId.length()-10);
			
		} catch (Exception e) {
			System.out.println("Login user method"+e);
			return null;
		}
	}
	
	
	public List<BookDetails> getAllBooks(){
		return jdbcTemplate.query("select * from bookdetails", new BookRowMapper());
	}
	
}

class BookRowMapper implements RowMapper<BookDetails>{

	@Override
	public BookDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
	
		BookDetails bd = new BookDetails();
		bd.setId(rs.getInt(1));
		bd.setTitle(rs.getString(2));
		bd.setUrl(rs.getString(3));
		return bd;
		
	}
	
}
