create database bookandlogin;
use bookandlogin;
create table logincredentials(username varchar(50) primary key not null,password varchar(50) not null);
insert into logincredentials values("nagarajan1667@gmail.com","nagaraj26");
insert into logincredentials values("harikrishnan234@gmail.com","harikrishnan289@gmail.com");
create table bookdetails(id int primary key not null,title varchar(60),url varchar(1000));
insert into bookdetails values(1,"Hansel & Gretel","https://5.imimg.com/data5/ANDROID/Default/2020/11/XX/PC/DZ/49559104/img-20201117-130616-jpg-500x500.jpg");
insert into bookdetails values(2,"Emma and the Lost Unicorn","https://www.writeratplay.com/wp-content/uploads/COVER.USE_.THIS_.Emma_-1.jpeg");
insert into bookdetails values(3,"Arabian Nights","https://img1.exportersindia.com/product_images/bc-full/dir_136/4065051/best-of-series-story-books-p-b-bw-1514537556-3549957.jpeg");
insert into bookdetails values(4,"Jack and the Bean Stalk","https://ae01.alicdn.com/kf/HTB1bYliKFXXXXc3XXXXq6xXFXXXk/education-book-Parragon-classic-warm-picture-books-famous-story-books-children-books-Parent-child-reading-Jack.jpg");
insert into bookdetails values(5,"Thunder in Love","https://gobooksdelivery.com/uploads/files/Thunder_in_love.jpg");
insert into bookdetails values(6,"Peter Pan","https://images-na.ssl-images-amazon.com/images/I/41JnDhCM%2BYL.jpg");
insert into bookdetails values(7,"The Perfect Nanny","https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1556564795-51R2Bt88MpRL.jpg");
insert into bookdetails values(8,"Shannon Hale","https://media.k-12readinglist.com/grade6/princess-academy.jpg");
insert into bookdetails values(9,"Gemma","https://images.squarespace-cdn.com/content/v1/5493706de4b0ecaa4047b871/1555537550311-U14PL43E83CTOFXHRJMI/Gemma+Cover+Site.jpg");
insert into bookdetails values(10,"Walk Two Moons","https://media.k-12readinglist.com/grade6/walk-two-moons.jpg");

select *from logincredentials;
select *from bookdetails;
